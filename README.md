# fuhur-api


Installs and configures Fuhur Agent service.

## Requirements

The role works together with ansible-role-fuhur-agent

## Role Variables

### Mandantory variables

```yaml
fuhur_sign_secret: "Change me"
fuhur_api_token: "Change me"
```

#### fuhur_sign_secret

That must be a random string which is shared with the agent('s). API will sign the jobs with that secret and agents will check if the signature is valid.

#### fuhur_api_token

API should be called with this secret

### Other variables

```yaml
fuhur_api_version: "v2.1.1"
fuhur_api_download_url: "https://gitlab.com/fuhur/api/-/jobs/artifacts/{{fuhur_api_version}}/download?job=fuhur_api"

fuhur_api_service_name: "fuhur-api"
fuhur_api_systemd_service_file_path: "/etc/systemd/system/{{ fuhur_api_service_name }}.service"
fuhur_api_systemd_socket_file_path: "/etc/systemd/system/{{ fuhur_api_service_name }}.socket"
```

## Example Playbook

```yaml
- hosts: fuhur-api-servers
  roles:
    - { role: fuhur.api, fuhur_sign_secret: "abcdefghijkl1234567890", fuhur_api_service_name: "0987654321lkjihgfedcba" }
```

## License

Apache License 2.0
